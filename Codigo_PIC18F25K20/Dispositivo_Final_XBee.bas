'***************************************************************
' Proyecto : Programación del Dispositivo Final XBee           *
' Fecha    : 13/06/2018                                        *
' Autor    : Jonas Carrillo                                    *
' Notas    : Código de cada uno de los sensores de los dispo-  *
'          : sitivos finales PIC18F25K20 - XBee-S2C            * 
'***************************************************************

':::::::::::::::::::::::::::LIBRERIAS:::::::::::::::::::::::::::

DelayMS 1000                             ' Tiempo de estabilización de la fuente 
Include "Amicus18.inc"                   ' Cargamos macros de AMICUS IDE
Include "Amicus18_Timers.Inc"            ' Cargamos macros Timer en el programa


':::::::::::::CONFIGURACIONES DE COMUNICACIÓN:::::::::::::::::::

TRISA = 0
TRISB = 127                              ' Configura PORTB como entrada EXCEPTO [RB 7]
PORTB = 127                              ' Inicializa PORTB
Declare Hserial_Baud = 9600              ' Seteamos la velocidad de comunicacion
Declare Hserial_Clear = On               ' Buffer limpiado despues de recibir


':::::::::::::::::::::VARIABLES Y CONSTANTES::::::::::::::::::::

Dim contador As Word                     ' Variable para control de registro del TIMER0
Dim tipo_sensor As Byte                  ' Variable para identificar el tipo de sensor activado
Dim sensor_mag_f As Byte                 ' Variable utilizada como bandera
Dim sensor_pir_f As Byte                 ' Variable utilizada como bandera
Dim sensor_hum_f As Byte                 ' Variable utilizada como bandera
Dim btnp_f As Byte                       ' Variable utilizada como bandera
Dim pulso_Led As Byte                    ' Variable para hacer el blink del LED conectado en PORTB.7
Dim cont_aux_led As Byte                 ' Variable utilizada para tiempo de encendido del LED
Symbol sensor_MAG = PORTB.0              ' Pin de lectura de sensor MAGNETICO
Symbol sensor_PIR = PORTB.1              ' Pin de lectura de sensor PIR
Symbol sensor_HUM = PORTB.2              ' Pin de lectura de sensor de HUMO
Symbol btnP = PORTB.3                    ' Pin de lectura de Boton de PANICO
Symbol blk = PORTB.7                     ' Pin de salida para conexión de LED

'------------ Inicialización de variables ---------------------
' Declaración del estado inicial de todas las variables

sensor_MAG = 1      ' Debido a que este sensor es activo en bajo
sensor_PIR = 0
sensor_HUM = 1      ' Debido a que este sensor es activo en bajo
btnP = 0
sensor_mag_f = 0
sensor_pir_f = 0
sensor_hum_f = 0
btnp_f = 0
contador = 0
tipo_sensor = 0
pulso_Led = 0
cont_aux_led = 0


On_Hardware_Interrupt GoTo Interrupt_blk  ' Declaración de Punto de interrupción 

'::::::::::::::::::: SALTO A LA RUTINA PRINCIPAL :::::::::::::::

GoTo Main

'::::::::::::::::::::::::::: SUBRUTINAS ::::::::::::::::::::::::

'======= Interrupción ======

Interrupt_blk:

    Context Save
    ' Fue un desbordamiento Timer0 que activó la Interrupción?
    If INTCONbits_T0IF = 1 Then
        Clear INTCONbits_T0IF ' Borrar el indicador de desbordamiento de Timer0
        
        '================== ECUACION ==================
        ' TMR0_Overflow = (2^n_bits-TMR0)(Prescaler)(4/FOsc)
                       '= (65536-65286)(128)(4/64MHz)
    
        '------------ base de tiempo---------------
        '------------------10ms--------------------
        
        WriteTimer0(64286)
        contador = contador + 1
        
        '------------ base de tiempo---------------
        '------------------10 s--------------------
        
        If contador == 1000 Then  
            GoSub envio_dato_Normal          ' Salto a subrrutina envio_dato_Normal
            Toggle blk
            pulso_Led = 1                    ' Enciendo LED en PORTB.7
            contador = 0
        EndIf
        
        If pulso_Led == 1 Then
            cont_aux_led = cont_aux_led + 1
            If cont_aux_led == 30 Then      ' Mantengo encendido el LED durante 300ms
                Toggle blk                  ' cambio el estdo del PORTB.7
                pulso_Led = 0
                cont_aux_led = 0
            EndIf
        EndIf        
    EndIf
Context Restore         ' Salgo de la interrupción, y restauro registros

'======= Envío normal de datos ========
' Descomentar línea de código
' Segun el sensor conectado al micro

envio_dato_Normal:

    HRSOut "I_ZON01_SMAG_EST:00_F",13,10
'    HRSOut "I_ZON01_SMOV_EST:00_F",13,10
'    HRSOut "I_ZON01_SHUM_EST:00_F",13,10
'    HRSOut "I_ZON01_BTNP_EST:00_F",13,10
    
Return

'======== Envío de datos de alerta ======
' Segun el sensor activado

envio_dato_Alerta:        ' Se llama a esta subrutina únicamente cuando 
                          ' se ha producido un evento detectado por el 
    Select tipo_sensor    ' Sensor conectado y se envía los datos por 
                          ' puerto serie
    Case 1
        HRSOut "I_ZON01_SMAG_EST:01_F",13,10
    
    Case 2
        HRSOut "I_ZON01_SMOV_EST:01_F",13,10
    
    Case 3
        HRSOut "I_ZON01_SHUM_EST:01_F",13,10
        
    Case 4
        HRSOut "I_ZON01_BTNP_EST:01_F",13,10
    End Select

Return

'::::::::::::::::::::::::RUTINA PRINCIPAL:::::::::::::::::::::::

Main:

    OpenTimer0(TIMER_INT_ON & T0_16BIT & T0_SOURCE_INT & T0_PS_1_128)
    INTCONbits_GIE = 1      ' Habilitamos interrupciones globales
    WriteTimer0 (64286)     ' Interrupcion cada 10ms
    
    
    While 1 = 1
 '::::::::::::::::::::: LECTURA DE SENSORES ::::::::::::::::::::
 
'--------------------------------------------------------------
'---------------- Lectura de  sensor MAG ----------------------

    If sensor_MAG == 0 Then     ' Lectrura de sensor MAGNETICO
        sensor_mag_f = 1        ' Cargamos valor en bandera del sensor 
    EndIf
'------------- Se ha activado el sensor MAG..?-----------------

    If sensor_mag_f == 1 Then   ' Existe cambio de estado
        tipo_sensor = 1
        GoSub envio_dato_Alerta ' Llamamos a subrutina 
        DelayMS 3000            ' Demora de 3 segundos para nueva lectura
        sensor_mag_f = 0        ' Reiniciamos bandera para un nuevo uso
        
    Else
        tipo_sensor = 0         ' Aseguramos siempre este estado por defecto
    EndIf
    
'--------------------------------------------------------------
'---------------- Lectura de  sensor PIR ----------------------

    If sensor_PIR == 1 Then     ' Lectrura de sensor PIR
        sensor_pir_f = 1        ' cargamos valor en bandera del sensor 
    EndIf

'------------- Se ha activado el sensor PIR..?-----------------

    If sensor_pir_f == 1 Then    ' Existe cambio de estado
        tipo_sensor = 2
        GoSub envio_dato_Alerta  ' Llamamos a subrutina
        DelayMS 3000             ' Demora de 3 segundos para nueva lectura
        sensor_pir_f = 0         ' Reiniciamos bandera para un nuevo uso
    Else
        tipo_sensor = 0          ' Aseguramos siempre este estado por defecto
    EndIf
    
'--------------------------------------------------------------
'---------------- Lectura de  sensor HUMO ---------------------

    If sensor_HUM == 0 Then      ' Lectrura de sensor HUMO
        sensor_hum_f = 1         ' cargamos valor en bandera del sensor 
    EndIf

'------------- Se ha activado el sensor HUMO..?----------------

    If sensor_hum_f == 1 Then    ' Existe cambio de estado
        sensor_hum_f = 0         ' Reiniciamos bandera para un nuevo uso
        tipo_sensor = 3
        GoSub envio_dato_Alerta  ' Llamamos a subrutina
        DelayMS 5000             ' Demora de 5 segundos para hacer una nueva lectura
    Else                         ' evitando envío masivo de datos a la central 
        tipo_sensor = 0          ' Aseguramos siempre este estado por defecto
    EndIf

'--------------------------------------------------------------
'---------------- Lectura de  BTN de pánico--------------------

    If btnP == 1 Then            ' Lectrura de botón de pánico
        btnp_f = 1               ' cargamos valor en bandera del sensor 
    EndIf

'----------- Se ha activado el BTN de Pánico..? ----------------

    If btnP == 0 And btnp_f == 1 Then    ' Boton pánico soltado (aseguramos antirebote)
        btnp_f = 0                       ' Reiniciamos bandera para un nuevo uso
        tipo_sensor = 4
        GoSub envio_dato_Alerta          ' Llamamos a subrutina
        DelayMS 3000
    Else
        tipo_sensor = 0                  ' Aseguramos siempre este estado por defecto
    EndIf
        
    Wend                                 ' Cerramos el ciclo while
