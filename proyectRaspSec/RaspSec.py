#!/usr/bin/env python3
# encoding: utf-8
# Archivo main
import sys
import argparse
from proyectRaspSec.configApp import (sensor_time, telegram_bot, telegram_API_KEY)
from proyectRaspSec.core.BootRaspSec.Boot import BootRaspSecurity


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_command_line(argv):
    """
    Analizar argumento de línea de comando. Ver opción -h
    :param argv: Los argumentos en la línea de comando deben incluir el nombre del archivo del llamante.
    :return:
    """
    formatter_class = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(description="¡Rasp Security!",
                                     epilog=telegram_bot,
                                     formatter_class=formatter_class)
    parser.add_argument("-l", "--logs", metavar="bool", default=False, help="Mostrar logs del Boot.")

    parser.add_argument('-aL', "--alert", metavar="bool", type=bool, default=0,
                        help="Activar alertas via SMS")

    parser.add_argument('-tS', "--timeSensor", metavar="int", type=float, default=sensor_time,
                        help="Tiempo en milisegundos para preguntar por el estado del sensor (2000 por defecto).")

    parser.add_argument('-pD', "--persistData", metavar="bool", type=bool,
                        help="Persistir los datos del chat")

    arguments = parser.parse_args(argv[1:])

    return arguments


def main():
    try:
        arguments = parse_command_line(sys.argv)
        if arguments:
            brs = BootRaspSecurity(telegram_API_KEY, arguments.timeSensor, arguments.logs)
            brs.run_boot()
    except KeyboardInterrupt:
        print('Programa interrumpido!')


if __name__ == "__main__":
    sys.exit(main())
