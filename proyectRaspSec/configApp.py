#!/usr/bin/env python3
# encoding: utf-8
from firebase_admin import credentials
import firebase_admin

"""
############
Config API TELEGRAM
############
"""
# Lina de comandos INICIO
comandos_telegram = {
    "login": {"command": "/login", "params": 1,
              "detail": "Ingresar usuario y PIN para tener acceso a las funcionalidades (Ejemplo: /login user:1233).",
              "name": "login"},
    "info": {"command": "/info", "params": 0, "detail": "Muestra información del Boot Rasp Security.",
             "name": "info"},
    "start": {"command": "/start", "params": 0, "detail": "Iniciar Boot.", "name": "start"},
    "stream": {"command": "/stream", "params": 1, "detail": "Notificar alertas en tiempo real.", "name": "stream"},
    "reporte": {"command": "/reporte", "params": 0, "detail": "Genera reporte del sistema.", "name": "reporte"},
    "foto": {"command": "/foto", "params": 0, "detail": "Solicitud de foto actual.", "name": "foto"},
    "salir": {"command": "/salir", "params": 0, "detail": "Cerrar sesión.", "name": "salir"},
    "acciones": {"command": "/acciones", "params": 0, "detail": "Acciones a ejecutar", "name": "Acciones"}
}
# Lina de comandos FIN
telegram_API_KEY = "549154724:AAEEk1X_fIUe_uE94ju1bAaXje_a_ORErQM"
telegram_name = "Rasp_Security"
telegram_bot = "@jonascsbot"
telegram_txt_menu = 'Hola *%s*, ingresa uno de los siguientes comandos para continuar:\n ' \
                    ''+comandos_telegram["stream"]["command"]+' - '+comandos_telegram["stream"]["detail"]+'\n ' \
                    ''+comandos_telegram["reporte"]["command"]+' - '+comandos_telegram["reporte"]["detail"]+'\n ' \
                    ''+comandos_telegram["foto"]["command"]+' - '+comandos_telegram["foto"]["detail"]+'\n ' \
                    ''+comandos_telegram["acciones"]["command"]+' - '+comandos_telegram["acciones"]["detail"]+'\n ' \
                    ''+comandos_telegram["salir"]["command"]+' - '+comandos_telegram["salir"]["detail"]+''

telegram_txt_login = 'Bienvenido al sistema de seguridad <b>'+telegram_name+'</b>, ' \
                       'ingrese uno de los siguientes comandos para iniciar:\n ' \
                       '<b>'+comandos_telegram["login"]["command"]+'</b> - '+comandos_telegram["login"]["detail"]+'\n ' \
                       '<b>'+comandos_telegram["info"]["command"]+'</b> - '+comandos_telegram["info"]["detail"]+'\n '

telegram_txt_info = 'Hola! Mi nombre es *'+telegram_bot+'*.'
options_alert = {"aop1": {"name": "On/Off Luces Ext.", "code": "aop1", "command": "Luces EXT ON/OFF",
                          "detail": "Encender luces extertas."},
                 "aop2": {"name": "On/Off Luces Int.", "code": "aop2", "command": "Luces INT ON/OFF",
                          "detail": "Encender luces internas"},
                 "aop3": {"name": "Reportar al ECU 911.", "code": "aop3", "command": "report-911",
                          "detail": "Reportar al ECU 911."},
                 "aop4": {"name": "On/Off Suministro Electrico", "code": "aop4", "command": "cut-electrical",
                          "detail": "Cortar suministro electrico de la casa"},
                 "aop5": {"name": "On/Off Suministro de Gas", "code": "aop5", "command": "cut-gas",
                          "detail": "Cortar suministro de gas de la cas-"},

                 }

"""
##############
List of pings
##############
"""
ping_list = {
    "pLightIn": {
        "code": "pLightIn",
        "ping": 2,
        "detail": "Pin para controlar la luz internas"
    },
    "pLightEx": {
        "code": "pLightEx",
        "ping": 3,
        "detail": "Pin para controlar la luz externas"
    },
    "pGas": {
        "code": "pGas",
        "ping": 4,
        "detail": "Pin para controlar suministro de gas"
    },
    "pElectric": {
        "code": "pElectric",
        "ping": 17,
        "detail": "Pin para controlar suministro de electricidad"
    }
}

"""
############
Config Sensors
############
"""
# Tiempo en segundos para analizar el estado de los sensores
sensor_time = 1
sensor_PID = 000


"""
############
Config Alert SMS
############
"""
sms_number = "099999998"
sms_text = "Alert!"


"""
############
Config Firebase
############
"""
fb_url_API = "https://raspsecurity.firebaseio.com/"
fb_delimitador = "/"
fb_data = {"delimitador": "/",
           "usuarios": {"name": "users"},
           "sesiones": {"name": "sessions"},
           "sensores": {"name": "sensors"},
           "tramas": {"name": "tramas"},
           }

if not len(firebase_admin._apps):
    # for test
    # cred = credentials.Certificate("../test-9fb38-firebase-adminsdk-51rye-1150639ccd.json")
    # for develop and production
    cred = credentials.Certificate("./test-9fb38-firebase-adminsdk-51rye-1150639ccd.json")
    default_app = firebase_admin.initialize_app(cred)

