import unittest
from proyectRaspSec.core.Services.SensorService import SensorService
from proyectRaspSec.core.Services.UserService import UserService


class TestModuleSensorService(unittest.TestCase):
    def test_save_sensor(self):
        ss = SensorService()
        ss.owner = "4DxzrXDNmTRWDKHK8T8gVfFmR302"
        res = ss.save_sensor()
        self.assertEqual(True, res)

    def test_gat_sensor(self):
        ss = SensorService()
        res = ss.set_data("4DxzrXDNmTRWDKHK8T8gVfFmR302")
        self.assertEqual(True, res)

    def test_check_user_have_sensor(self):
        us = UserService()
        us.get_user("jonascs1692@.com")
        ss = SensorService()
        res = ss.check_sensor(us)
        self.assertEqual(True, res)

    if __name__ == '__main__':
        unittest.main()
