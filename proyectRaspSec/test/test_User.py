import unittest
from proyectRaspSec.core.Services.UserService import UserService
from proyectRaspSec.core.models.UserModels import UserModel


class TestModuleUserService(unittest.TestCase):

    def test_post_user_on_firebase(self):
        u = UserService()
        u.pin = 123456
        u.nombre = "Jonas"
        u.apellido = "Carrillo"
        u.telefono = "+593982730064"
        u.uid = "asdfsdrtf43fwae435q22"
        u.uid_chat = "dwefasgasdgewrt345435rwet2"
        u.token = "dbfca484-b075-4b8c-81d1-b6be0aeee242"
        u.username = "jonascs1692@gmail.com"
        a = u.save_user()
        self.assertEqual(True, a, "Correcto!")

    def test_update_user_on_firebase(self):
        u = UserService()
        u.get_user("jonascs1692@gmail.com")
        u.token = "345436434564"
        a = u.update_user()
        self.assertEqual(True, a, "Correcto!")

    if __name__ == '__main__':
        unittest.main()


class TestModuleUserModel(unittest.TestCase):

    def test_post_user_on_firebase(self):
        u = UserModel()
        a = u.get_user("jonascs1692@gmail.com")
        self.assertEqual(dict(), type(a), "Correcto!")

    if __name__ == '__main__':
        unittest.main()
