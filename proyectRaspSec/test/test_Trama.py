import unittest
from proyectRaspSec.core.Services.TramaService import TramaService


class TestModuleTramaService(unittest.TestCase):
    def test_save_trama(self):
        t = TramaService("INS_2_DSFSD_322", "4DxzrXDNmTRWDKHK8T8gVfFmR302")
        res = t.save_trama()
        self.assertEqual(True, res)


if __name__ == '__main__':
    unittest.main()
