
class DataTrama:

    def __init__(self, data):
        self.txt = data
        self.zona = self.get_zona()
        self.sensor = self.get_sensor()
        self.estado = self.get_estado()
        self.end_trama = self.get_end_trama()

    def get_zona(self):
        return self.txt[2:7]

    def get_sensor(self):
        return self.txt[8:12]

    def get_estado(self):
        return self.txt[13:19]

    def get_end_trama(self):
        return self.txt[20:22]

    def __str__(self):
        return "DataTrama(Zona: {}, Sensor; {}, Estado: {}, End_trama: {})".format(self.zona, self.sensor, self.estado, self.end_trama)
