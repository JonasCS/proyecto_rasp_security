import serial
from ..Sensors.Trama import DataTrama


class SensorCentral():
    def __init__(self):
        self.serial = serial.Serial()
        self.serial.nombre = nombre
        self.serial.port = port
        self.serial.baudrate = baudrate
        self.serial.timeout = timeout
        self.trama = object
        self.estado = ""

    def get_trama(self):
        if not self.serial.isOpen():
            self.serial.open()
        if self.serial.isOpen():
            self.trama = DataTrama(self.serial.readline().decode("utf-8"))
            #self q.serial.close()
            return self.trama
        else:
            raise Exception("Port no open")

    def close_serial(self):
        self.serial.close()
