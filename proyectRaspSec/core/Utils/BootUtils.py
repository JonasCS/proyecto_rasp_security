from proyectRaspSec.configApp import comandos_telegram


def command_is(cmd, text=""):
    """
    Funcion que permite verificar si un comando esta correcto, y devolver valores dependiendo el comando
    :param cmd: Nombre de comando que se va a analizar.
    :param text: Texto con el comando a analizar.
    :return: Diccionario de datos dependiendo del comando.
    """
    command = text.split()[0]
    if command == comandos_telegram[cmd]["command"]:
        if len(text.split()) == (comandos_telegram.get(cmd)["params"]+1):
            if command == comandos_telegram.get("login")["command"]:
                if command == comandos_telegram["login"]["command"]:
                    user_pass = text.split()[1]
                    return {"user": user_pass.split(":")[0], "pin": user_pass.split(":")[1]}
                else:
                    return False
            elif command == comandos_telegram.get("stream")["command"]:
                res = text.split()[1]
                if res.lower() == "activar":
                    return {"status": True}
                elif res.lower() == "desactivar":
                    return {"status": False}
                else:
                    print("Sintaxis de comando erroneo!")
                    raise Exception("Sintaxis de comando erroneo!")
            elif command == comandos_telegram.get("start")["command"]:
                return True
            else:
                return False
        else:
            print("Sintaxis de comando erroneo!")
            raise Exception("Sintaxis de comando erroneo!")
    else:
        return False


def command_is_permited(text):
    """
    Funcion que permite verificar si un comando estan en una lista de comandos permitidos
    :param text: Comando con el texto recibido
    :return:
    """
    text = text.split()[0]
    comandos_permitidos = list()
    comandos_permitidos.append(comandos_telegram.get("login"))
    comandos_permitidos.append(comandos_telegram.get("info"))
    comandos_permitidos.append(comandos_telegram.get("start"))
    comandos_permitidos.append(comandos_telegram.get("stream"))
    for comando in comandos_permitidos:
        if comando["command"] in text:
            return True
    return False
