from picamera import PiCamera
import datetime
import time
import os


class Camara:
    pwd = os.getcwd()
    sleep_take = 3

    def __init__(self, uid):
        self.path = ""
        self.uid = uid

    def take_photo(self):
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
        cam = PiCamera()
        cam.start_preview()
        dir_uid = str(self.pwd) + "/" + str(self.uid)
        if not os.path.exists(dir_uid):
            os.makedirs(dir_uid)
        path_photo = dir_uid + "/" + str(timestamp) + ".jpg"
        print("path_photo:")
        print(path_photo)
        time.sleep(self.sleep_take)
        cam.capture(path_photo)
        self.path = path_photo
        cam.stop_preview()
        cam.close()

    def get_dir(self):
        return self.path
