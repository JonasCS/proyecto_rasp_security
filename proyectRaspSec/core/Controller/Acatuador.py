import RPi.GPIO as GPIO
import time


class Actuador:
    GPIO.setmode(GPIO.BCM)

    def __init__(self, pin):
        self.pin = pin
        GPIO.setup(pin, GPIO.OUT)

    @staticmethod
    def clear():
        GPIO.cleanup()

    def turn_off(self):
        print("turn_off")
        GPIO.output(self.pin, GPIO.HIGH)

    def turn_on(self):
        print("turn_on")
        GPIO.output(self.pin, GPIO.LOW)

    def status(self):
        return GPIO.input(self.pin)

    def toggle(self):
        st = int(self.status())
        print("st:", str(st))
        if st == 0:
            self.turn_off()
        elif st == 1:
            self.turn_on()
        else:
            self.clear()
