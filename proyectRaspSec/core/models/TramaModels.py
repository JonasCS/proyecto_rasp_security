from proyectRaspSec.configApp import cred, default_app
from firebase_admin import firestore
import firebase_admin
import datetime


class TramaModel:
    """
    Modelo de una trama usada para los servicios de firebase
    """
    db = firestore.client(default_app)

    def __init__(self, data: str, sendor_id: str):
        trama = data.split("_")
        self.date = datetime.datetime.now()
        self.zona = trama[1]
        self.sensor = trama[2]
        self.estado = trama[3]
        self.end_trama = trama[4]
        self.sendor_id = sendor_id

    @staticmethod
    def from_dict(source):
        """
        D.A. Función para obtener un objeto del mismo modelo.
        :param source:
        :return:
        """
        trama = TramaModel(source["data"])
        trama.date = source["date"] if "date" in source else ""
        trama.zona = source["zona"] if "zona" in source else ""
        trama.sensor = source["sensor"] if "sensor" in source else ""
        trama.estado = source["estado"] if "estado" in source else ""
        trama.end_trama = source["end_trama"] if "end_trama" in source else ""
        trama.sendor_id = source["sendor_id"] if "sendor_id" in source else ""
        return trama

    def to_dict(self):
        """
        D.A. Función para retornar un diccionario del objeto
        :return:
        """
        return self.__dict__

    def __repr__(self):
        return "DataTrama(Date: {}, Zona: {}, Sensor; {}, Estado: {}, End_trama: {}, Sendor_id: {})"\
            .format(self.date, self.zona, self.sensor, self.estado, self.end_trama, self.sendor_id)
