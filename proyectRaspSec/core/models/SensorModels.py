from proyectRaspSec.configApp import cred, default_app
from firebase_admin import firestore
import firebase_admin
import datetime


class SensorModel:
    """
    Modelo de un sensor usada para los servicios de firebase
    """
    db = firestore.client(default_app)

    def __init__(self, id_sensor="", name="Central", port="/dev/ttyUSB0", baudrate=9600, timeout=1):
        self.id_sensor = id_sensor
        self.name = name
        self.port = port
        self.date = datetime.datetime.now()
        self.baudrate = baudrate
        self.timeout = timeout
        self.owner = ""
        self.status = False

    @staticmethod
    def from_dict(source):
        """
        D.A. Función para obtener un objeto del mismo modelo.
        :param source:
        :return:
        """
        sensor = SensorModel()
        sensor.id_sensor = source["id_sensor"] if "id_sensor" in source else ""
        sensor.name = source["name"] if "name" in source else ""
        sensor.port = source["port"] if "port" in source else ""
        sensor.date = source["date"] if "date" in source else ""
        sensor.baudrate = source["baudrate"] if "baudrate" in source else ""
        sensor.timeout = source["timeout"] if "timeout" in source else ""
        sensor.owner = source["owner"] if "owner" in source else ""
        sensor.status = source["status"] if "status" in source else ""
        return sensor

    def to_dict(self):
        """
        D.A. Función para retornar un diccionario del objeto
        :return:
        """
        return self.__dict__

    def __repr__(self):
        return "Sensor(Id= {}, name={}, port={}, date={}, baudrate={}, timeout={}, owner={}, status={})"\
            .format(self.id_sensor, self.name, self.port, self.date, self.baudrate, self.timeout, self.owner,
                    self.status)
