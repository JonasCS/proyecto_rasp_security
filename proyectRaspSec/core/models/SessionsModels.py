from proyectRaspSec.configApp import cred, default_app
from firebase_admin import firestore
import firebase_admin
import datetime


class SessionModel:
    """
    Modelo de una session usada para los servicios de firebase
    """
    db = firestore.client(default_app)

    def __init__(self):
        self.last_date = datetime.datetime.now()
        self.username = ""
        self.uid_chat = ""
        self.pin = ""
        self.token = ""

    @staticmethod
    def from_dict(source):
        """
        D.A. Función para obtener un objeto del mismo modelo.
        :param source:
        :return:
        """
        session = SessionModel()
        session.last_date = source["last_date"] if "last_date" in source else ""
        session.username = source["username"] if "username" in source else ""
        session.uid_chat = source["uid_chat"] if "uid_chat" in source else ""
        session.pin = source["pin"] if "pin" in source else ""
        session.token = source["token"] if "token" in source else ""
        return session

    def to_dict(self):
        """
        D.A. Función para retornar un diccionario del objeto
        :return:
        """
        return self.__dict__

    def __repr__(self):
        return "Session(last_date={}, username={}, uid_chat={}, pin={}, token={})"\
            .format(self.last_date, self.username, self.uid_chat, self.pin, self.token)
