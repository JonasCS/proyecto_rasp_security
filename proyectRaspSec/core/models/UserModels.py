from proyectRaspSec.configApp import cred, fb_data, default_app
from firebase_admin import firestore
import firebase_admin


class UserModel:
    """
    Modelo de un Usuario usada para los servicios de firebase
    """
    db = firestore.client(default_app)

    def __init__(self):
        self.pin = None
        self.nombre = ""
        self.apellido = ""
        self.telefono = ""
        self.uid = ""
        self.uid_chat = ""
        self.username = ""

    @staticmethod
    def from_dict(source):
        """
        D.A. Función para obtener un objeto del mismo modelo.
        :param source:
        :return:
        """
        user = UserModel()
        user.pin = source["pin"] if "pin" in source else ""
        user.nombre = source["nombre"] if "nombre" in source else ""
        user.apellido = source["apellido"] if "apellido" in source else ""
        user.telefono = source["telefono"] if "telefono" in source else ""
        user.uid = source["uid"] if "uid" in source else ""
        user.uid_chat = source["uid_chat"] if "uid_chat" in source else ""
        user.username = source["username"] if "username" in source else ""
        return user

    def to_dict(self):
        """
        D.A. Función para retornar un diccionario del objeto
        :return:
        """
        return self.__dict__

    def __repr__(self):
        return "User(pin={}, nombre={}, apellido={}, telefono={}, uid={}, uid_chat={}, username={})"\
            .format(self.pin, self.nombre, self.apellido, self.telefono, self.uid, self.uid_chat, self.username)
