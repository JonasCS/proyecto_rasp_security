from proyectRaspSec.configApp import fb_url_API
from proyectRaspSec.core.Services.UserService import UserService
from proyectRaspSec.core.Services.SessionService import SessionService
from uuid import uuid4
import pickle
import os
# from firebase.firebase import FirebaseApplication, FirebaseAuthentication


class LoginUser:
    """
    Clase para simular un sistema de logeo con Telegram
    """
    path_session = "/tmp/sessions"

    def __init__(self, username=None, pin=None):
        self.user = username
        self.pin = pin

    def login_cache(self, unique_code):
        """
        D.A. Función para saber si un usuario esta logeado en cache.
        :param unique_code: id del usario que se va a registrar
        :return: Retorna False o True
        """
        list_user = []
        if os.path.exists(self.path_session):
            with open(self.path_session, 'rb') as cache_file:
                list_user = pickle.load(cache_file)
                cache_file.close()
            for user in list_user:
                if user["uid_chat"] == int(unique_code):
                    return user
                else:
                    return False
        else:
            return False

    def logout(self, update):
        """
        D.A. Función para eliminar la session local y remotamente (firebase)
        :param update: Objeto update de telegram bot
        :return:
        """
        uid = str(update.effective_user.id)
        ss = SessionService()
        ss.get_session(uid)
        ss.token = ""
        ss.update_session()

        #eliminar registro local
        list_user = []
        if os.path.exists(self.path_session):
            with open(self.path_session, 'rb') as cache_file:
                list_user = pickle.load(cache_file)
                cache_file.close()
            for i, user in enumerate(list_user):
                if str(user["uid_chat"]) == str(uid):
                    list_user.remove(user)
                    print("logout:", str(user["uid_chat"]))
                else:
                    print("usuario no encontrado en cache:", str(user["uid_chat"]))

            with open(self.path_session, 'wb') as cache_file:
                pickle.dump(list_user, cache_file)
                cache_file.close()

    def save_session_user(self, data_user):
        """
        D.A. Función para registrar un usurio logeado localmente para evitar peticones a firebase
        :param data_user: id del usario
        :return:
        """
        list_user = []
        if os.path.exists(self.path_session):
            with open(self.path_session, 'rb') as cache_file:
                list_user = pickle.load(cache_file)
                cache_file.close()
            exist_user = False
            for user in list_user:
                if user["uid"] == data_user["uid"]:
                    exist_user = True

            if not exist_user:
                list_user.append(data_user)

            with open(self.path_session, 'wb') as cache_file:
                pickle.dump(list_user, cache_file)
                cache_file.close()
        else:
            list_user.append(data_user)
            with open(self.path_session, 'wb') as cache_file:
                pickle.dump(list_user, cache_file)
                cache_file.close()

    def authentication(self, update):
        """
        Función que permite autenticar al usuario y devolver el token de sesión
        :return:
        """
        us = UserService()
        exists_user = us.get_user(self.user)
        if exists_user:
            if str(self.pin) == str(exists_user.pin):
                us.token = str(uuid4())
                us.uid_chat = update.message.chat_id
                us.update_user()
                print("login:")
                print(us.to_dict())

                ss = SessionService()
                ss.username = us.username
                ss.token = us.token
                ss.uid_chat = str(update.message.chat_id)
                ss.save_session()
                self.save_session_user(us.to_dict())
                return us.to_dict()
            else:
                return False
        else:
            return False
