from functools import wraps
from ..Services.SessionService import SessionService
from ..Services.UserService import UserService
from ..BootRaspSec.LoginUser import LoginUser
from telegram import ParseMode, ChatAction


def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        """
        D.A. Función para verificar que la persona tenga los permisos para ejecutar cualquier comando.
        :param bot:
        :param update:
        :param args:
        :param kwargs:
        :return:
        """
        user_id = args[0].effective_user.id
        lu = LoginUser()
        user_exist = lu.login_cache(str(user_id))
        if user_exist:
            kwargs["user_data"] = user_exist
            return func(bot, update, *args, **kwargs)
        else:
            # En caso de que la sesion en cache no esta gurdad se consulta a la base de datos.
            ss = SessionService()
            ss.get_session(str(user_id))
            if ss.token:
                lu = LoginUser()
                us = UserService()
                user_model = us.get_user_uid_chat(user_id)
                lu.save_session_user(user_model.to_dict())
                kwargs["user_data"] = user_model.to_dict()
                return func(bot, update, *args, **kwargs)
            else:
                print("Unauthorized access denied for {}.".format(user_id))
                update.send_message(chat_id=args[0].message.chat_id, text="`¡Permisos Insuficientes!`",
                                 parse_mode=ParseMode.MARKDOWN)
    return wrapped


def send_typing_action(func):
    """Sends typing action while processing func command."""

    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.send_chat_action(chat_id=args[0].effective_message.chat_id, action=ChatAction.TYPING)
        return func(update, context,  *args, **kwargs)

    return command_func


def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(update, context, *args, **kwargs)

        return command_func

    return decorator
