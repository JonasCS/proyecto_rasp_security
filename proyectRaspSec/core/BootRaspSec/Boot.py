import logging
from telegram import ReplyKeyboardMarkup, ParseMode
from proyectRaspSec.core.Utils.BootUtils import *
from proyectRaspSec.configApp import *
import json
from ..BootRaspSec.LoginUser import LoginUser
from ..Services.SensorService import SensorService
from ..models.UserModels import UserModel
from ..BootRaspSec.wraps import restricted, send_typing_action
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler, ConversationHandler,
                          DispatcherHandlerStop, CallbackQueryHandler)

from ..Controller.Acatuador import Actuador
from ..Controller.Camara import Camara
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


class BootRaspSecurity:
    # Enable logging
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)

    logger = logging.getLogger(__name__)

    CHOOSING, TYPING_REPLY, TYPING_CHOICE = range(3)

    reply_keyboard = [['Encender Luces Ext.', 'Encender Luces Int.'],
                      ['Reportar al ECU 911', 'Cortar Suministro Electrico'],
                      ['Cortar Suministro de Gas']]
    markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)

    def __init__(self, token, sensor_time, logs_boot=False):
        self.API_TOKEN = token
        self.sensor_time = sensor_time
        self.logs = logs_boot

    @staticmethod
    def facts_to_str(user_data):
        facts = list()

        for key, value in user_data.items():
            facts.append('{} - {}'.format(key, value))

        return "\n".join(facts).join(['\n', '\n'])

    def cmd_start(self, bot, update):
        bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_info,
                         parse_mode=ParseMode.MARKDOWN)
        bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_login,
                         parse_mode=ParseMode.HTML)

    def regular_choice(self, bot, update, user_data):
        text = update.message.text
        user_data['choice'] = text
        update.message.reply_text(
            'Your {}? Yes, I would love to hear about that!'.format(text.lower()))

        return self.TYPING_REPLY

    def custom_choice(self, bot, update):
        update.message.reply_text('Alright, please send me the category first, '
                                  'for example "Most impressive skill"')

        return self.TYPING_CHOICE

    def received_information(self, bot, update, user_data):
        text = update.message.text
        category = user_data['choice']
        user_data[category] = text
        del user_data['choice']

        update.message.reply_text("Neat! Just so you know, this is what you already told me:"
                                  "{}"
                                  "You can tell me more, or change your opinion on something."
                                  .format(self.facts_to_str(user_data)), reply_markup=self.markup)

        return self.CHOOSING

    def done(self, bot, update, user_data):
        if 'choice' in user_data:
            del user_data['choice']

        update.message.reply_text("I learned these facts about you:"
                                  "{}"
                                  "Until next time!".format(self.facts_to_str(user_data)))

        user_data.clear()
        return ConversationHandler.END

    @staticmethod
    def echo(bot, update, user_data):
        bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_login, parse_mode=ParseMode.HTML)

    @send_typing_action
    def cmd_login(self, bot, update):
        res = command_is("login", update.message.text)
        if res:
            lu = LoginUser(res["user"], res["pin"])
            auth = lu.authentication(update)
            if auth:
                bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_menu % auth["username"],
                                 parse_mode=ParseMode.MARKDOWN)
            else:
                bot.send_message(chat_id=update.message.chat_id, text="`¡Usuario o pin incorrecto!`",
                                 parse_mode=ParseMode.MARKDOWN)
        else:
            bot.send_message(chat_id=update.message.chat_id, text="`Comando Incorrecto!`",
                             parse_mode=ParseMode.MARKDOWN)

    def check_allowed(self, bot, update):
        # Agregar funcion para preguntar si el usuario ya esta logeado para no logear a cada momento
        # pasar key
        # https://github.com/python-telegram-bot/python-telegram-bot/wiki/Storing-user--and-chat-related-data
        is_login = False
        if not is_login:
            if command_is_permited(update.message.text):
                res = command_is("login", update.message.text)
                if res:
                    # Si se procede a un logeo
                    # user = User()
                    # user_allow = user.get_user(update.message.from_user.id)
                    bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_menu % res["user"],
                                     parse_mode=ParseMode.MARKDOWN)
                else:
                    # Otra opcion
                    pass
            else:
                bot.send_message(chat_id=update.message.chat_id, text="`Comando no permitido!`",
                                 parse_mode=ParseMode.MARKDOWN)
                bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_login,
                                 parse_mode=ParseMode.HTML)
                raise DispatcherHandlerStop
        else:
            bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_login,
                             parse_mode=ParseMode.HTML)
            raise DispatcherHandlerStop

    def error(self, bot, update, error):
        """Log Errors caused by Updates."""
        self.logger.warning('Update "%s" caused error "%s"', update, error)

    @restricted
    def cmd_salir(self, bot, update, user_data):
        lu = LoginUser()
        lu.logout(update)
        bot.send_message(chat_id=update.message.chat_id, text="Has cerrado sessión.")

    @restricted
    def cmd_stream(self, bot, update, user_data, job_queue, args):
        status = ""
        try:
            status = command_is(comandos_telegram.get("stream")["name"], update.message.text)
        except Exception:
            bot.send_message(chat_id=update.message.chat_id, text="Envie la opción 'Activar' o 'Desactivar'")
            status = False
            return
        if status:
            if status["status"]:
                # activar el stream
                ss = SensorService()
                um = UserModel()
                if ss.check_sensor(um.from_dict(user_data)):
                    ss.status = True
                    ss.update_sensor()
                    ss.set_data(user_data["uid"])
                    job_queue.run_repeating(ss.callback_sensor, interval=self.sensor_time, first=0, context=update)
                    bot.send_message(chat_id=update.message.chat_id, text="Notificaciones en tiempo real activadas..")
                else:
                    print("No tiene sensores asignados")
                    bot.send_message(chat_id=update.message.chat_id,
                                     text="No tiene sensores asignados")
            else:
                # desactivar el stream
                pass
        else:
            bot.send_message(chat_id=update.message.chat_id, text="Envie la opción 'Activar' o 'Desactivar'")

    @restricted
    def cmd_video(self, bot, update, user_data):
        bot.send_message(chat_id=update.message.chat_id, text="Servicio de %s." % comandos_telegram["video"]["name"])

    @restricted
    def cmd_reporte(self, bot, update, user_data):
        bot.send_message(chat_id=update.message.chat_id, text="Servicio de %s." % comandos_telegram["reporte"]["name"])

    @restricted
    def cmd_info(self, bot, update, user_data):
        bot.send_message(chat_id=update.message.chat_id, text=telegram_txt_menu % user_data["username"])

    @restricted
    def cmd_foto(self, bot, update, user_data):
        bot.send_message(chat_id=update.message.chat_id, text="Servicio de %s." % comandos_telegram["foto"]["name"])
        foto = Camara(user_data["uid"])
        foto.take_photo()
        bot.send_photo(chat_id=update.message.chat_id, photo=open(foto.get_dir(), 'rb'))

    @restricted
    def cmd_acciones(self, bot, update, user_data):
        keyboard = [[InlineKeyboardButton(option["name"], callback_data=json.dumps(
            {"code": option["code"], "owner":user_data["uid"]}))] for option in options_alert.values()]

        reply_markup = InlineKeyboardMarkup(keyboard)
        bot.send_message(chat_id=update.message.chat_id, text="Acciones", reply_markup=reply_markup)

    def button(self, update, context):
        query = context.callback_query
        query_data = json.loads(query.data)
        if query_data["code"] in options_alert:
            option_alert_slected = options_alert.get(query_data["code"])
            query.edit_message_text(text="Opción seleccionada: {}".
                                    format(option_alert_slected["detail"]))
            print("write in sensor!!")
            ss = SensorService()
            ss.get_sensor(query_data["owner"])
            ss.writer_sensor(option_alert_slected["command"])
            print("Writing in sensor {}".format(option_alert_slected["command"]))
            print("query_data[code]", str(query_data["code"]))
            if query_data["code"] == "aop2":
                pin = ping_list.get("pLightIn")
                luces_internas = Actuador(pin["ping"])
                luces_internas.toggle()
            elif query_data["code"] == "aop1":
                pin = ping_list.get("pLightEx")
                luces_externas = Actuador(pin["ping"])
                luces_externas.toggle()
            elif query_data["code"] == "aop4":
                pin = ping_list.get("pElectric")
                electric = Actuador(pin["ping"])
                electric.toggle()
            elif query_data["code"] == "aop5":
                pin = ping_list.get("pGas")
                gas = Actuador(pin["ping"])
                gas.toggle()

    def run_boot(self):
        # Create the Updater and pass it your bot's token.
        updater = Updater(self.API_TOKEN)


        # Get the dispatcher to register handlers
        dp = updater.dispatcher

        # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.cmd_start)],

            states={
                self.CHOOSING: [RegexHandler('^(Age|Favourite colour|Number of siblings)$', self.regular_choice,
                                             pass_user_data=True), RegexHandler('^Something else...$',
                                                                                self.custom_choice),
                                ],
                self.TYPING_CHOICE: [MessageHandler(Filters.text, self.regular_choice, pass_user_data=True), ],
                self.TYPING_REPLY: [MessageHandler(Filters.text, self.received_information, pass_user_data=True), ],
            },

            fallbacks=[RegexHandler('^Done$', self.done, pass_user_data=True)]
        )

        # dp.add_handler(MessageHandler(Filters.all, self.check_allowed), -1)

        dp.add_handler(conv_handler)

        echo_handler = MessageHandler(Filters.text, self.echo, pass_user_data=True)
        dp.add_handler(echo_handler)

        updater.dispatcher.add_handler(CallbackQueryHandler(self.button))

        dp.add_handler(CommandHandler(comandos_telegram["login"]["name"], self.cmd_login))
        dp.add_handler(CommandHandler(comandos_telegram["salir"]["name"], self.cmd_salir, pass_user_data=True))
        dp.add_handler(CommandHandler(comandos_telegram["stream"]["name"], self.cmd_stream, pass_user_data=True,
                                      pass_job_queue=True, pass_args=True))
        dp.add_handler(CommandHandler(comandos_telegram["video"]["name"], self.cmd_video, pass_user_data=True))
        dp.add_handler(CommandHandler(comandos_telegram["reporte"]["name"], self.cmd_reporte, pass_user_data=True))
        dp.add_handler(CommandHandler(comandos_telegram["info"]["name"], self.cmd_info, pass_user_data=True))
        dp.add_handler(CommandHandler(comandos_telegram["foto"]["name"], self.cmd_foto, pass_user_data=True))
        dp.add_handler(CommandHandler(comandos_telegram["acciones"]["name"], self.cmd_acciones, pass_user_data=True))

        # Escribir todos los errores
        if self.logs:
            dp.add_error_handler(self.error)

        # Iniciar el Boot
        if updater.start_polling():
            print("Boot started...")

        # Para cerrar bien el boot y eliminar todos los procesos
        updater.idle()
