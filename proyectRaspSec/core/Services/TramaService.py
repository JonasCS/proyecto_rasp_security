from proyectRaspSec.configApp import *
from proyectRaspSec.core.models.TramaModels import TramaModel
from firebase_admin import firestore


class TramaService(TramaModel):
    db = firestore.client(default_app)

    def __init__(self, data: str, sensor_id: str):
        super().__init__(data, sensor_id)

    def update_trama(self):
        pass

    def get_trama(self):
        pass

    def save_trama(self):
        user_collection = self.db.collection(fb_data["tramas"]["name"]).document()
        res = user_collection.set(self.to_dict())
        return True if res else False
