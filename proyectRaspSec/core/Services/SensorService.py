from proyectRaspSec.configApp import *
from proyectRaspSec.core.models.SensorModels import SensorModel
from proyectRaspSec.core.Services.TramaService import TramaService
from firebase_admin import firestore
import serial
import json
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


class SensorService(SensorModel):
    db = firestore.client(default_app)

    def __init__(self):
        super().__init__()
        self.serial = serial.Serial()

        self.serial.nombre = self.name
        self.serial.port = self.port
        self.serial.baudrate = self.baudrate
        self.serial.timeout = self.timeout

    def update_sensor(self):
        """
        D.A. Funcion para actualizar un sensor en la base de datos.
        :return:
        """
        pass

    def set_data(self, user):
        """
        D.A. Función para lenar el modelo con datos
        :param user: Modelo of user of firebase o un string del uid del usuario
        :return: True o False
        """
        all_sensors = self.db.collection(fb_data["sensores"]["name"])
        print("user:", user)
        result = all_sensors.where("owner", "==", user if type(user) is str else user.uid)
        if len(list(result.get())) > 1:
            raise Exception("No suporte para multiples sensores")
        else:
            print("len: ", str(len(list(result.get()))))
            sensor = list(result.get())[0].to_dict()
            self.id_sensor = list(result.get())[0].id
            self.name = sensor["name"]
            self.port = sensor["port"]
            self.date = sensor["date"]
            self.baudrate = sensor["baudrate"]
            self.timeout = sensor["timeout"]
            self.owner = sensor["owner"]
            self.status = sensor["status"]
            return True

    def get_sensor(self, user):
        """
        D.A. Función para obtener los datos de firebase y establecerlos en el modelo
        :param user: Modelo of user of firebase o un string del uid del usuario
        :return: List of Sensor Models
        """
        list_sensors = []
        all_sensors = self.db.collection(fb_data["sensores"]["name"])
        result = all_sensors.where("owner", "==", user if type(user) is str else user.uid)
        for sensor in result.get():
            list_sensors.append(self.from_dict(sensor.to_dict()))
        return list_sensors

    def check_sensor(self, user):
        """
        D.A. Esta función permite verificar si un usuario tiene un sensor asignado.
        :return: True o False
        """
        return True if len(self.get_sensor(user)) > 0 else False

    def save_sensor(self):
        """
        D.A. Función para agregar una nueva session
        :return:
        """
        user_collection = self.db.collection(fb_data["sensores"]["name"]).document()
        res = user_collection.set(self.to_dict())
        return True if res else False

    def callback_sensor(self, bot, job):
        keyboard = [[InlineKeyboardButton(option["name"], callback_data=json.dumps(
            {"code": option["code"], "owner":self.owner}))] for option in options_alert.values()]

        reply_markup = InlineKeyboardMarkup(keyboard)

        if not self.serial.isOpen():
            self.serial.open()
        if self.serial.isOpen():
            datos_readline = str(self.serial.readline().decode("utf-8")).replace(" ", "")
            if datos_readline:
                if not self.id_sensor:
                    raise Exception("Error: id_sensor no definido para trama")
                trama_sensor = TramaService(datos_readline, self.id_sensor)
                if trama_sensor.estado == "EST:01":
                    trama_sensor.save_trama()
                    bot.send_message(chat_id=job.context.message.chat_id, text='¡Alerta! Zona {} activada'
                                     .format(trama_sensor.zona))
                    job.context.message.reply_text('¿Que desea hacer? ', reply_markup=reply_markup)

                elif trama_sensor.estado == "EST:02" or trama_sensor.estado == "EST:03" or \
                        trama_sensor.estado == "EST:04" or trama_sensor.estado == "EST:05":
                    trama_sensor.save_trama()

                else:
                    print("Estado desconocido:")
                    print(datos_readline)
            else:
                print("Nada que mostrar en el sensor", datos_readline)
        else:
            raise Exception("Port no open")

    def close_serial(self):
        if self.serial.isOpen():
            self.serial.close()

    def writer_sensor(self, command):
        if not self.serial.isOpen():
            self.serial.open()
        self.serial.write(command.encode())
        self.close_serial()
