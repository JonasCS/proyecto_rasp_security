from proyectRaspSec.configApp import *
from proyectRaspSec.core.models.UserModels import UserModel
from firebase_admin import firestore
from firebase_admin import auth
import google.cloud.exceptions
import sys


class UserService(UserModel):
    db = firestore.client(default_app)

    def __init__(self):
        super().__init__()

    def update_user(self):
        """
        D.A. Funcion para actualizar un usuario en la base de datos, necesita al menos una usuario
        :return:
        """
        if self.username:
            user_collection = self.db.collection(fb_data["usuarios"]["name"]).document(self.username)
            user_collection.update(self.to_dict())

    def save_user(self):
        """
        D.A. Función para agregar un nuevo usuario
        :return:
        """
        if self.username:
            user_collection = self.db.collection(fb_data["usuarios"]["name"]).document(self.username)
            user_collection.set(self.to_dict())

    def get_user(self, username):
        try:
            u = auth.get_user_by_email(username, default_app)
            doc_ref = self.db.collection(fb_data["usuarios"]["name"]).document(u.email)

            doc = doc_ref.get()
            if not doc.exists:
                return False
            else:
                data = doc.to_dict()
                try:
                    self.pin = data["pin"] if "pin" in data else ""
                    self.nombre = data["nombre"] if "nombre" in data else ""
                    self.apellido = data["apellido"] if "apellido" in data else ""
                    self.telefono = data["telefono"] if "telefono" in data else ""
                    self.uid = data["uid"] if "uid" in data else ""
                    self.uid_chat = data["uid_chat"] if "uid_chat" in data else ""
                    self.username = data["username"] if "username" in data else ""
                    return self
                except KeyError as e:
                    raise Exception("Clave no encontrada: %s" % e.args[0])
        except google.cloud.exceptions.NotFound:
            print("Usuario no encontrado!")
            return False
        except:
            print("Usuario no encontrado! (%s)" % sys.exc_info()[0])
            return False

    def get_user_uid_chat(self, uid):
        """
        D.A. Funcion para retornar un objeto UserModel
        :param uid: uid del usuario
        :return: Retornar objeto UserModel
        """
        all_sensors = self.db.collection(fb_data["usuarios"]["name"])
        result = all_sensors.where("uid_chat", "==", uid)
        for user in result.get():
            return self.from_dict(user.to_dict())

    def delete_user(self):
        pass

