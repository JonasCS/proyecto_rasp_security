from proyectRaspSec.configApp import *
from proyectRaspSec.core.models.SessionsModels import SessionModel
from firebase_admin import firestore
from firebase_admin import auth
import google.cloud.exceptions


class SessionService(SessionModel):
    db = firestore.client(default_app)

    def __init__(self):
        super().__init__()

    def update_session(self):
        """
        D.A. Funcion para actualizar una session en la base de datos, necesita al menos una usuario
        :return:
        """
        if self.username:
            session_collection = self.db.collection(fb_data["sesiones"]["name"]).document(self.uid_chat)
            session_collection.update(self.to_dict())

    def get_session(self, uid):
        """
        D.A. Función para obtener los datos de firebase y establecerlos en el modelo
        :param uid:
        :return:
        """
        doc_ref = self.db.collection(fb_data["sesiones"]["name"]).document(uid)
        try:
            doc = doc_ref.get()
            if not doc.exists:
                return False
            else:
                data = doc.to_dict()
                try:
                    self.last_date = data["last_date"] if "last_date" in data else ""
                    self.username = data["username"] if "username" in data else ""
                    self.uid_chat = data["uid_chat"] if "uid_chat" in data else ""
                    self.pin = data["pin"] if "pin" in data else ""
                    self.token = data["token"] if "token" in data else ""
                    return self
                except KeyError as e:
                    raise Exception("Clave no encontrada: %s" % e.args[0])
        except google.cloud.exceptions.NotFound:
            print("Session no encontrado!")
            return False

    def save_session(self):
        """
        D.A. Función para agregar una nueva session
        :return:
        """
        if self.username:
            user_collection = self.db.collection(fb_data["sesiones"]["name"]).document(self.uid_chat)
            user_collection.set(self.to_dict())
