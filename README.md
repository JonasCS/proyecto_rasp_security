# Rasp Security
## Requerimientos para código de la central del sistema
Se recomienda usar una version de Python3.5 o superior.
- Python3.5 (Version usada)
- VirtualEnv

## Levantar el entorno
```bash
sudo apt install python3-pip
pip3 install virtualenv --user
mkdir ~/virtaulEnv
python3 -m virtualenv ~/virtualEnv/raspSecEnv
source ~/virtualEnv/raspSecEnv/bin/activate
#Entrar en la carpeta del proyecto
pip install -r requirements.txt
```
## Configuración
Para iniciar correctamente la aplicacion se tiene que copiar la llave de firebase en el directorio `proyecto_rasp_security/projectRaspSec/`.

## Ejecutar Script
Entrar al proyecto y ejecutar el script inicial.
- Exportar directorio del paquete ejecutados el sigueinte comandos desde el directorio `proyecto_rasp_security`
```bash
export PYTHONPATH=`pwd`
```
- Despues entrar a la carpeta del proyecto y ejecutar el script incial
```bash
python RaspSec.py
```

# Requerimientos para programación del PIC18F25K20
Se recomienda utilizar la versión de PROTON IDE 
- V 2.0.3.3
- mas Proton BASIC Compiler 3.5.9.3



Descargar [PROTON IDE](http://www.protonbasic.co.uk/content.php/141-Proton-Development-Suite)